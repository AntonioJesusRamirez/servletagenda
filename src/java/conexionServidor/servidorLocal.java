package conexionServidor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio
 */
@WebServlet(name = "servidorLocal", urlPatterns = {"/servidorLocal"})
public class servidorLocal extends HttpServlet {
    
    public static final byte PETICION_GET = 0;    // SELECT
    public static final byte PETICION_POST = 1;   // INSERT
    public static final byte PETICION_PUT = 2;    // UPDATE
    public static final byte PETICION_DELETE = 3; // DELETE
    
    private byte tipoPeticion;

    private Logger logger = Logger.getLogger("");
    
//    Personas personas = new Personas();
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JAXBException {
//        response.setContentType("text/html;charset=UTF-8");
        response.setContentType("text/xml;charset=UTF-8");
        PrintWriter out = response.getWriter();
//        try (PrintWriter out = response.getWriter()) {
        try {
            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet servidorLocal</title>");            
//            out.println("</head>");
//            out.println("<body>");

            // Connection with database using an entity manager
            EntityManager entityManager = 
                     Persistence.createEntityManagerFactory("WebApplication1PU")
                                    .createEntityManager();

            // Execute a query (a generated named query in entity class)
            Query query = entityManager.createNamedQuery("Persona.findAll");
            
            // Get an objects list as result of previous query
            Personas personas = new Personas();
            personas.getArrayListPersonas().addAll(query.getResultList());
            
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "Esta dentro del servel");
            switch (tipoPeticion) {
                case PETICION_GET:
                    jaxbMarshaller.marshal(personas, out);
                    Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "Dentro del Get");
                    // Iterate the list getting every object
//                    for (Persona persona : personas.getArrayListPersonas()) {
//                        out.println("<h1>" + persona.getNombre() + " " + persona.getApellidos()
//                                + " " + persona.getDni() + " " + persona.getCP() + "</h1>");
//                        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, persona.getNombre());
//                    }
                    break;
                case PETICION_POST:
                    // Obtener la lista de peronas que se quieren insertar.
                    personas = (Personas) jaxbUnmarshaller.unmarshal
                            (request.getInputStream());
                    Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, String.valueOf(personas.getArrayListPersonas().size()));
                    // Recorrer la lista obteniendo cada objeto contenido en ella.
                    for(Persona persona :  personas.getArrayListPersonas()) {
                        // Añadir cada objeto a la lista general.
                        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, persona.getNombre() + "Dentro del Post");
                        entityManager.getTransaction().begin(); 
                        entityManager.persist(persona); 
                        entityManager.getTransaction().commit();   
                    }
                    break;
                case PETICION_PUT:
                    
                    Personas editarPersonas = (Personas) jaxbUnmarshaller.unmarshal
                            (request.getInputStream());
                    // Recorrer la lista obteniendo cada objeto contenido en ella.
                    for(Persona persona :  editarPersonas.getArrayListPersonas()) {
                    //  cada objeto a la lista general.
                        personas.getArrayListPersonas().set(persona.getId(), persona);
                        entityManager.getTransaction().begin(); 
                        entityManager.merge(persona); 
                        entityManager.getTransaction().commit();   
                    
                    }
                    
                    break;
                case PETICION_DELETE:
                    Personas borrarPersonas = (Personas) jaxbUnmarshaller.unmarshal
                            (request.getInputStream());
                    // Recorrer la lista obteniendo cada objeto contenido en ella.
                    for(Persona persona :  borrarPersonas.getArrayListPersonas()) {
                        entityManager.getTransaction().begin(); 
                        persona = entityManager.find(Persona.class, persona.getId());
                        entityManager.remove(persona); 
                        entityManager.getTransaction().commit();   
                        // borramos el objeto persona de la lista de individuos
                        personas.getArrayListPersonas().remove(persona);
                        
                    }
                    break;
                default:
                    break;
            }     



//            out.println("</body>");
//            out.println("</html>");
//            generarXML(personas);
            // Close connection
//            entityManager.close();
 
        } catch (JAXBException ex) {
            Logger.getLogger(Personas.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            out.close();
    }
        

    
    /**
     * Genera un XML.
     * @param ArrayListPersonas es un arrayList con todos los datos de cada 
     * persona. 
     */
    /*
    public void generarXML(Personas ArrayListPersonas){
     
        try {
            Personas personas = new Personas();
            personas = ArrayListPersonas;
            // Se indica el nombre de la clase que contiene la lista de objetos
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // Indicar que se desea generar el xml con saltos de línea y tabuladores
            //  para facilitar su lectura
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Generar XML mostrándolo por la salida estándar
            jaxbMarshaller.marshal(personas, System.out);

        } catch (JAXBException ex) {
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    */

    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_GET;
        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doGet1");
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doGet", ex);
        }
    }

    /**
    * Handles the HTTP <code>POST</code> method.
    *
    * @param request servlet request
    * @param response servlet response
    * @throws ServletException if a servlet-specific error occurs
    * @throws IOException if an I/O error occurs
    */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_POST;
        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doPost1");
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doPost", ex);
        }
    }

        /**
     * Handles the HTTP <code>DELETE</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        tipoPeticion = PETICION_DELETE;
        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doDelete1");
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doDelete", ex);
        }
    }

    /**
     * Handles the HTTP <code>PUT</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        tipoPeticion = PETICION_PUT;
        Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doPut1");
        try {
            processRequest(request, response);
        } catch (JAXBException ex) {
            Logger.getLogger(servidorLocal.class.getName()).log(Level.SEVERE, "falla en el doPut", ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
